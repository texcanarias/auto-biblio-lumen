CREATE DATABASE auto_biblio;
USE auto_biblio;

#Tabla para documentos
CREATE TABLE documents (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    name_file varchar(255) NOT NULL,
    mime varchar(50) NOT NULL,
    description blob,
    PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8; 

#Tabla para tags
CREATE TABLE tags(
    id int NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8; 

CREATE TABLE rel_documents_tags(
    documents_id int NOT NULL,
    tags_id int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=UTF8; 

ALTER TABLE rel_documents_tags ADD FOREIGN KEY (documents_id) REFERENCES documents(id);
ALTER TABLE rel_documents_tags ADD FOREIGN KEY (tags_id) REFERENCES tags(id);
