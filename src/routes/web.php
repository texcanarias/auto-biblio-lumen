<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return "API v1.0 auto-biblio-lumen";
});


$router->get(   'documents/',           'Documents\GetController@execute');
$router->post(  'documents/',           'Documents\PostController@execute');
$router->get(   'documents/{id}',       'Documents\GetByIdController@execute');
$router->delete('documents/{id}',       'Documents\DeleteController@execute');
$router->get(   'documents/tag/{tag}',  'Documents\Tag\GetController@execute');
