<?php
namespace App\Http\Controllers\Documents;

use App\Http\Controllers\Controller;
use App\Exceptions\Documents\{ExceptionInputMimeRequired, ExeptionInputNameRequired};
use Illuminate\Http\Response;
use scan\document\models\{Document,Tag,ArrayTag};

class PostController extends Controller
{
    public function execute(\Illuminate\Http\Request $request){
        try{
            $document = $this->entrada($request);
        } 
        catch(ExceptionInputMimeRequired $ex){
            $data = array('error' => 'input mime is required');
            return (new Response($data, 400))
                ->header('Content-Type', 'application/json');
        }
        catch(ExceptionInputNameRequired $ex){
            $data = array('error' => 'input name is required');
            return (new Response($data, 400))
                ->header('Content-Type', 'application/json');
        }
        catch(ExceptionInputNameFileRequired $ex){
            $data = array('error' => 'input name file is required');
            return (new Response($data, 400))
                ->header('Content-Type', 'application/json');    
        }
        catch(\Exception $ex){
            $data = array('error' => $ex->getMessage());
            return (new Response($data, 400))
                ->header('Content-Type', 'application/json');
        }

        //Negocio
        try{
            $document = \scan\document\services\PostService::execute(
                                                \scan\document\messages\PostMessage::create($document),
                                                \App\Persistence\Documents\PersistenceDocument::create());
        } catch(\Exception $ex){
            echo $ex->getMessage();
        }

        return $this->salida($document);        
    }

    private function entrada(\Illuminate\Http\Request $request) : \scan\document\models\Document{
        // Get all POST parameters
        $data = $request->json()->all();
        if(JSON_ERROR_NONE !== json_last_error ( )){
            throw new \Exception("Json error " . json_last_error_msg());
        }

        // Get a single POST parameter
        $mime = $data['mime'];
        if(null == $mime){
            throw new ExceptionInputMimeRequired;
        }

        $name = $data['name'];
        if(null == $name){
            throw new ExceptionInputNameRequired;
        }

        $nameFile = $data['name_file'];
        if(null == $nameFile){
            throw new ExceptionInputNameFileRequired;
        }

        $tags = $data['tags'];
        if(null == $tags){
            $tags = "";
        }

        $docTags = new ArrayTag();
        foreach($tags as $tag){
            $docTags->add(Tag::factoryNew(null,$tag));
        }

        try{
            $document = Document::factoryFromArrayTag(null,$name,$nameFile,$mime,$docTags);
        } catch(\Exception $ex){
            echo $ex->getMessage();
        }

        return $document;
    }

    private function salida(\scan\document\models\Document $document) : Response{
        $payload = json_encode($document);
        return (new Response($payload, 400))
            ->header('Content-Type', 'application/json');
    }

}
