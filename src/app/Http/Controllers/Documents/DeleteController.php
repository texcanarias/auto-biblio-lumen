<?php
namespace App\Http\Controllers\Documents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class DeleteController extends Controller
{
    use \App\Http\Controllers\Documents\TraitEntradaId;
 
    public function execute(\Illuminate\Http\Request $request, int $id){
        try{
            $documentId = $this->entrada($id);
        } 
        catch(ExceptionInputNumericError $ex){
            return (new Response(['error' => 'input error is not numeric'], 400))
                ->header('Content-Type', 'application/json');   
        }

        //Llamada a servicios
        try {
            $document = \scan\document\services\DeleteService::execute(
                \scan\document\messages\DeleteMessage::create($documentId),
                \App\Persistence\Documents\PersistenceDocument::create());
        } catch (\Exception $ex) {
            return (new Response([], 500))
                ->header('Content-Type', 'application/json');   

        }

        return $this->salida($document);
    }

    private function salida(\scan\document\models\Document $document): Response
    {
        //Transformacion de datos
        $arrayDocument = ['id' => $document->getId(),
            'mime' => $document->getMime(),
            'name' => $document->getName(),
            'name_file' => $document->getNameFile(),
            'tags' => $document->getTags()];

        //Salida
        $statusCode = (null == $document->getId()) ? 404 : 200;
        return (new Response($arrayDocument, $statusCode))
            ->header('Content-Type', 'application/json');                
    }
    
}
