<?php
namespace App\Http\Controllers\Documents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class GetByIdController extends Controller
{
    use \App\Http\Controllers\Documents\TraitEntradaId;

    public function execute(\Illuminate\Http\Request $request, int $id){
        try{
            $documentId = $this->entrada($id);
        } 
        catch(ExceptionInputNumericError $ex){
            return (new Response(['error' => 'input error is not numeric'], 400))
            ->header('Content-Type', 'application/json');
        }

        //Negocio
        try{
            $document = \scan\document\services\GetByIdService::execute(
                                                        \scan\document\messages\GetByIdMessage::create($documentId) , 
                                                        \App\Persistence\Documents\PersistenceDocument::create());
        } catch (\Exception $ex){
            return (new Response([], 500))
            ->header('Content-Type', 'application/json'); 
        }
   
        return $this->salida($document);
    }

    private function salida(\scan\document\models\Document $document) : Response{
        $statusCode = (null == $document->getId())?404:200;
        return (new Response($document->jsonSerialize(), $statusCode))
        ->header('Content-Type', 'application/json');                   
    }    
}
