<?php
declare(strict_types=1); // strict mode
namespace App\Http\Controllers\Documents;

use  Slim\Psr7\Request;
use  Slim\Psr7\Response;
use Slim\Routing\RouteContext;

trait TraitEntradaId{
    private function entrada(int $id) : int{
        if(0 === $id){
            throw new ExceptionInputNumericError();
        }
        return $id;
    }

}