<?php
declare(strict_types=1); // strict mode
namespace App\Http\Controllers\Documents;

use  Slim\Psr7\Request;
use  Slim\Psr7\Response;
use  Illuminate\Http\Response;

trait TraitSalidaArrayDocument{
    private function salida(\scan\document\models\ArrayDocument $documents) : Response{
        $collection = [];
        foreach($documents as $document){
            //Transformacion de datos
            $collection[] = [  'id' => $document->getId(),
                                'mime' =>  $document->getMime(),
                                'name' => $document->getName(),
                                'name_file' => $document->getNameFile(),
                                'tags' => $document->getTags()];
        }


         //Salida
         return (new Response($collection, 200))
         ->header('Content-Type', 'application/json');                 
    }

}