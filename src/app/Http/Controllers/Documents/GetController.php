<?php
namespace App\Http\Controllers\Documents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class GetController extends Controller
{
    public function execute(){
        try{
            $documents = \scan\document\services\GetService::execute(\App\Persistence\Documents\PersistenceArrayDocument::create());
        } catch (\Exception $ex){
            return (new Response([], 500))
                  ->header('Content-Type', 'application/json');    
        }

        $collection = [];
        foreach($documents as $document){
            //Transformacion de datos
            $collection[] = [  'id' => $document->getId(),
                                'mime' =>  $document->getMime(),
                                'name' => $document->getName(),
                                'name_file' => $document->getNameFile(),
                                'tags' => $document->getTags()];
        }

        //Salida
        return (new Response($collection, 200))
            ->header('Content-Type', 'application/json');    


    }
}
