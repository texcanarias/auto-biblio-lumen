<?php
namespace App\Http\Controllers\Documents\Tag;

use App\Http\Controllers\Controller;

class GetController extends Controller
{
    public function execute(){
        $tag = $this->entrada($request);
 
        try{
            $documents = \biblio\services\documents\tag\GetService::execute(
                                                                            \biblio\messages\documents\GetMessagesByTag::create($tag) , 
                                                                            \biblio\persistence\documents\PersistenceArrayDocument::create()
                                                                        );
        }
        catch(\Exception $ex){
            //TODO Devolver error 500
            throw $ex;
        }

        return $this->salida($documents, $response); 
        }

        private function entrada(Request $request): string
        {
            $routeContext = RouteContext::fromRequest($request);
            $route = $routeContext->getRoute();        
            $tag = $route->getArgument('tag');
            return $tag;
        }
    }
}
