<?php
declare(strict_types=1); // strict mode
namespace App\Persistence\Documents;

use scan\framework\persistences\pdo\Connect as PerConnect;

class Connect{
    public static function create(){
        $connect = new PerConnect('db_auto_biblio_lumen','auto_biblio','root','root');
        return $connect;
    }
}