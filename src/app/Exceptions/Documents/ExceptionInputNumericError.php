<?php
declare(strict_types=1); // strict mode
namespace App\Exceptions\Documents;

class ExceptionInputNumericError extends \Exception{
    public function __construct(){
        return parent::__construct("Numeric error");
    }
}